# erupcja-js [WIP]

SDK for UONET+ school gradebook (with a focus on Hebe API)

MRs are welcome

huge thanks to:

- [wulkanowy/uonet-request-signer](https://github.com/wulkanowy/uonet-request-signer/tree/master/hebe-node)

related:

- [erupcja/uonetplus-hebe-certificate-generator](https://framagit.org/erupcja/uonetplus-hebe-certificate-generator)
- [erupcja/fcm-listener-node](https://framagit.org/erupcja/fcm-listener-node)

chat on matrix: [#erupcja-js:laura.pm](https://matrix.to/#/#erupcja-js:laura.pm) / [#erupcja:laura.pm](https://matrix.to/#/#erupcja:laura.pm)
