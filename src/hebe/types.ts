export interface HebeResponse<T> {
  EnvelopeType: string;
  Envelope: T;
  Status: { Code: 0; Message: 'OK' };
  RequestId: string; // uuid
  Timestamp: number; // unix timestamp
  TimestampFormatted: string; // '2020-05-12 01:06:57'
  InResponseTo: null;
}

// /api/mobile/register/new
export interface HebeAccountNew {
  RestURL: string; // 'https://uonetplus-komunikacja.eszkola.opolskie.pl/opole/'
  LoginId: number;
  UserLogin: string; // e-mail
  UserName: string; // e-mail
}

// /api/mobile/register/hebe
export interface HebeAccountHebe {
  Capabilities: HebeCapabilities[];
  ClassDisplay: string; // '2et'
  ConstituentUnit: {
    Address: string; // 'ul.Tadeusza Kościuszki 39-41, 45-062 Opole'
    Id: number;
    Name: string; // 'Publiczne Technikum Nr 5 w Zespole Szkół Elektrycznych im. Tadeusza Kościuszki w Opolu';
    Patron: string | null;
    SchoolTopic: string; // uuid
    Short: string; // 'PT5'
  };
  Educators: [
    {
      Id: string;
      Initials: string; // 'MK'
      LoginId: number;
      Name: string; // first name
      Roles: [
        {
          Address: string; // 'Makise Kurisu [MK] - wychowawca 2et (PT5)'
          AddressHash: string;
          ClassSymbol: string; // '2et (PT5)'
          ConstituentUnitSymbol: string; // 'PT5'
          Initials: string; // 'MK'
          Name: string; // 'Kurisu'
          RoleName: string; // 'Wychowawca'
          RoleOrder: string; // 0
          Surname: string; // 'Makise'
          UnitSymbol: null;
        },
      ];
      Surname: string; // 'Makise'
    },
  ];
  FullSync: boolean;
  InfoDisplay: string; // 'OpoleZSE - et2'
  Journal: {
    Id: number;
    YearEnd: HebeTime;
    YearStart: HebeTime;
  };
  Login: {
    DisplayName: string; // 'Laura Karaś'
    FirstName: string; // 'Laura'
    Id: number;
    LoginRole: string; // 'Uczen'
    SecondName: string | null; // 'Lena'
    Surname: string; // 'Karaś'
    Value: string; // 'laura@erupcja.science'
  };
  Partition: string; // 'opole-OpoleZSE'
  Periods: HebePeriod[];
  Pupil: {
    FirstName: string; // 'Laura'
    Id: number;
    LoginId: number;
    LoginValue: string; // 'laura@erupcja.science'
    SecondName: string; // 'Lena'
    Sex: boolean | null; // true -> male, false -> female (?)
    Surname: string; // 'Karaś'
  };
  SenderEntry: {
    Address: string; // 'Karaś Laura - uczeń 2et (PT5)'
    AddressHash: string;
    Initials: string; // 'KL'
    LoginId: number;
  };
  TopLevelPartition: string; // 'opole'
  Unit: {
    Address: string; // 'ul.Tadeusza Kościuszki 39-41, 45-062 Opole'
    DisplayName: string; // 'Zespół Szkół Elektrycznych   im.Tadeusza Kościuszki'
    Id: number;
    Name: string; // 'Zespół Szkół Elektrycznych  '
    Patron: string; // 'Tadeusz Kościuszko'
    RestURL: string; // 'https://uonetplus-komunikacja.eszkola.opolskie.pl/opole/OpoleZSE/api'
    Short: string; // 'OpoleZSE'
    Symbol: string; // 'OpoleZSE'
  };
}

export enum HebeCapabilities {
  REGULAR = 'REGULAR',
  TOPICS_ENABLED = 'TOPICS_ENABLED',
  ADDRESS_BOOK_PUPIL = 'ADDRESS_BOOK_PUPIL',
}

export interface HebeTime {
  Date: string; // '2019-08-31'
  DateDisplay: string; // '31.08.2019'
  Time: string; // '00:00:00'
  Timestamp: number; // 1567202400000
}

export interface HebePeriod {
  Current: boolean;
  End: HebeTime;
  Id: number;
  Last: boolean;
  Level: number; // school year
  Number: number; // period (semestr) of a school year
  Start: HebeTime;
}
