import got from 'got';
import { v4 as uuidv4 } from 'uuid';
import { getUnixTime, format, addSeconds } from 'date-fns';
import { register as registerFirebase } from 'fcm-listener';
import { getSignatureValues } from '@wulkanowy/uonet-request-signer-node-hebe';
import generateHebeKeys from '@erupcja/uonetplus-hebe-certificate-generator-node';
import { IGradebookAPI, IHebeCredentials, IHebeRegisterCredentials } from '../types';
import { HebeResponse, HebeAccountNew } from './types';
import {
  UONETPLUS_TOKEN_ROUTING_RULES_URL,
  FAKELOG_ROUTING_RULES,
  UONETPLUS_DATE_PATTERN_1,
  UONETPLUS_GOOGLE_DATA,
} from '../constant-values';

export default class UonetplusHebeAPI /* implements IGradebookAPI */ {
  credentials: IHebeCredentials;

  constructor(credentials: IHebeCredentials) {
    this.credentials = credentials;
  }

  /*
  async basicRequest() {}

  async getGrades() {
    return [];
  }
  */
}

export async function register({ token, pin, symbol }: IHebeRegisterCredentials) {
  const [key, server, firebase] = await Promise.all([
    generateHebeKeys(), // haha laptop fans go brrrrr

    (async () => {
      const routingRules = await got.get(UONETPLUS_TOKEN_ROUTING_RULES_URL).then((res) =>
        res.body
          .split('\n')
          .map((l) => l.trim())
          .concat(FAKELOG_ROUTING_RULES),
      );
      const routingRule = routingRules.find((l) => l.substring(0, 3) === token.substring(0, 3));
      if (!routingRule) {
        throw new Error('Invalid UONET+ token (no matching route)');
      }
      return routingRule.substring(4);
    })(),

    registerFirebase(UONETPLUS_GOOGLE_DATA),
  ]);
  const time = new Date();
  const body = JSON.stringify({
    API: 1,
    AppName: 'DzienniczekPlus 2.0',
    AppVersion: '1.0',
    CertificateId: key.fingerprint,
    Envelope: {
      Certificate: `${key.certificate}\n`,
      CertificateThumbprint: key.fingerprint,
      CertificateType: 'X509',
      DeviceModel: 'Xiaomi Redmi Note 3',
      OS: 'Android',
      PIN: pin,
      SecurityToken: token,
      SelfIdentifier: uuidv4(),
    },
    FirebaseToken: firebase.firebaseToken,
    RequestId: uuidv4(),
    Timestamp: getUnixTime(time),
    TimestampFormatted: format(time, UONETPLUS_DATE_PATTERN_1),
  });
  const requestPath = `${server}/${symbol}/api/mobile/register/new`;
  const { canonicalUrl, digest, signature } = getSignatureValues(
    key.fingerprint,
    key.privateKey,
    body,
    requestPath,
    time.getTime(),
  );
  const newDevice = await got
    .post(requestPath, {
      body,
      headers: {
        vOS: 'Android',
        vDeviceModel: 'Redmi Note 3',
        vAPI: '1',
        vDate: new Date(time.getTime() + 1000).toUTCString(),
        vCanonicalUrl: canonicalUrl,
        Digest: digest,
        Signature: signature,
        'User-Agent': 'okhttp/3.11.0',
        'Content-Type': 'application/json; charset=utf-8',
      },
    })
    .then((res) => JSON.parse(res.body) as HebeResponse<HebeAccountNew>)
    .then((res) => res.Envelope);
  return {
    key,
    registerData: {
      newDevice,
    },
  };
}
