import { IStudent } from './types';

export default class ErStudent {
  data: IStudent;

  constructor(student: IStudent) {
    this.data = student;
  }
}
