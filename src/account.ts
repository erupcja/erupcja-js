import { IAccount, IStudent } from './types';

export default class ErAccount {
  students: IStudent[];

  constructor(account: IAccount) {
    this.students = account.students;
  }
}
