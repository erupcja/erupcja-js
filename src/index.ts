import got from 'got';
import ErAccount from './account';
import { IHebeRegisterCredentials } from './types';
import { UONETPLUS_TOKEN_ROUTING_RULES_URL, FAKELOG_ROUTING_RULE } from './constant-values';

export default class Erupcja {
  accounts: ErAccount[] = [];
}
