export interface IAccount {
  students: IStudent[];
}

export interface IStudent {
  account: IAccount;
  getGrades: () => Promise<any[]>;
}

export interface IGradebookAPI {
  getGrades?: () => Promise<any[]>;
}

export interface IHebeRegisterCredentials {
  token: string;
  pin: string;
  symbol: string;
}

export interface IHebeCredentials {}
